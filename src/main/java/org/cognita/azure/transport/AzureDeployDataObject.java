package org.cognita.azure.transport;

/**
 * @author Prashant Admile
 *
 */
public class AzureDeployDataObject {
	
	private String client;
	private String tenant;
	private String key;
	private String subscriptionKey;
	private String rgName;
	private String acrName;
	/**
	 * @return the client
	 */
	public String getClient() {
		return client;
	}
	/**
	 * @param client the client to set
	 */
	public void setClient(String client) {
		this.client = client;
	}
	/**
	 * @return the tenant
	 */
	public String getTenant() {
		return tenant;
	}
	/**
	 * @param tenant the tenant to set
	 */
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the subscriptionKey
	 */
	public String getSubscriptionKey() {
		return subscriptionKey;
	}
	/**
	 * @param subscriptionKey the subscriptionKey to set
	 */
	public void setSubscriptionKey(String subscriptionKey) {
		this.subscriptionKey = subscriptionKey;
	}
	
	
	
	/**
	 * @return the rgName
	 */
	public String getRgName() {
		return rgName;
	}
	/**
	 * @param rgName the rgName to set
	 */
	public void setRgName(String rgName) {
		this.rgName = rgName;
	}
	/**
	 * @return the acrName
	 */
	public String getAcrName() {
		return acrName;
	}
	/**
	 * @param acrName the acrName to set
	 */
	public void setAcrName(String acrName) {
		this.acrName = acrName;
	}
	public String toString() {
		return "client : "+getClient() + " tenant : " + getTenant() + " key : "+ getKey() + " subscrptionKey :" + getSubscriptionKey() ;
	}
	

}
