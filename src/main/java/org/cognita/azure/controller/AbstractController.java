package org.cognita.azure.controller;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author Ashwin Sharma
 *
 */
public abstract class AbstractController {

	protected static final String APPLICATION_JSON = "application/json";

	protected final ObjectMapper mapper;

	public AbstractController() {
		mapper = new ObjectMapper();
	}

}
