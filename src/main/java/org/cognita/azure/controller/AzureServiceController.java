package org.cognita.azure.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cognita.azure.api.APINames;
import org.cognita.azure.service.impl.AzureServiceImpl;
import org.cognita.azure.transport.AzureDeployDataObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.microsoft.azure.management.Azure;


@RestController
public class AzureServiceController extends AbstractController {
	
	
	
	@RequestMapping(value = {org.cognita.azure.api.APINames.AZURE_AUTH_PUSH_IMAGE}, method = RequestMethod.POST)
	public String authorizeAndPushImage(HttpServletRequest request, @RequestBody AzureDeployDataObject authObject, HttpServletResponse response) {
		
		try {
			if (authObject == null) {
				System.out.println("Insufficient data to authneticate with Azure AD");
				return APINames.AUTH_FAILED;
			}

			AzureServiceImpl azureImpl=new AzureServiceImpl();
			Azure azure = azureImpl.authorize(authObject);
			
			
			/*
			 * Authorization done, noe try to push the image
			  if(azure!=null) {
				azureImpl.pushImage(azure,authObject.getClient(),authObject.getKey(),authObject.getRgName(),authObject.getAcrName() );
			}*/
			response.setStatus(200);	
			
		} catch (com.microsoft.aad.adal4j.AuthenticationException e) {
			System.out.println(e.getMessage() + " Returning... " + APINames.AUTH_FAILED );
			response.setStatus(401);
			return APINames.AUTH_FAILED;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage() + " Returning... " + APINames.FAILED );
			response.setStatus(500);
			return APINames.FAILED;
		}

		return APINames.SUCCESS_RESPONSE;
	}	
	

}


