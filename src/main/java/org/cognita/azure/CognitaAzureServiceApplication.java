package org.cognita.azure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CognitaAzureServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CognitaAzureServiceApplication.class, args);
	}
}
