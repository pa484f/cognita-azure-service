package org.cognita.azure.api;


/**
 * @author Prashant Admile
 *
 * Constants class to list all the REST API endpoints
 */
public class APINames {

	// charset
    public static final String CHARSET = "application/json;charset=utf-8";
    public static final String SUCCESS_RESPONSE="SUCCESS";
    public static final String AUTH_FAILED="AUTHORIZATION FAILED";
    public static final String FAILED="FAILED";

    //Service APIs 
    public static final String AZURE_AUTH_PUSH_IMAGE = "/azure/authAndpushimage";
   
}
