package org.cognita.azure.service;

import org.cognita.azure.transport.AzureDeployDataObject;

import com.microsoft.azure.management.Azure;

public interface AzureService {
	
	/*
	 *  Authorize with microsoft Azure active directory
	 */
	Azure authorize(AzureDeployDataObject authObject);
	
	/*
	 * Push/Deploy the image to application
	 */	

	boolean pushImage(Azure azure, String clientId, String secret, String rgName, String acrName);
	
}
